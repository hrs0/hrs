%**start of header
\documentclass[orivec,oribibl,runningheads]{llncs}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{xspace}
\usepackage{theorem}
\theorembodyfont{\rmfamily}

\usepackage{fancyvrb}
\usepackage{header}

\textheight=23cm

\begin{document}
%**end of header

\title{Definition of Higher-Order Rewrite System}

\date{}
\author{Makoto Hamana}
\institute{Department of Computer Science, Gunma University, Japan\\
  \email{hamana@cs.gunma-u.ac.jp}
}
\maketitle

\section{Definition of Higher-Order Rewrite System}

We describe Mayr and Nipkow's framework in detail for CoCo.

\subhead{Types}
A set of \W{base types} \BB is given.
The set $\TT$ of \W{(simple) types} is
generated by the following rules:
\[
\infrule{Atomic}
{b \in \BB}{b\in\Ty}
\quad
\infrule{Arr}
{\sig,\tau \in \Ty }
{\sig \to \tau \in \TT}
\]

\subhead{Signature} A \W{signature} \Sig is given by
a set of function symbol declarations of the form
\[
f: \tau
\]
where $f$ is a function symbol, and $\tau \in \TT$.
Function symbols must be different from variables.

\subhead{Variables}
Variables are given by a type-indexed family $\set{V_\tau}_{\tau\in\TT}$
such that
\begin{enumerate}
\item $V_\tau$ is a \W{countably infinite set} of variables
for each $\tau \in \TT$,
\item $V_\sig \cap V_\tau=\emptyset$ if $\sig\not=\tau$.
\end{enumerate}

Writing $x :\tau$ for a variable $x$, we mean
$x \in V_\tau$.
\Remark
The same variable name of different types, such as $x:\fn{int}$ and $x:\fn{char}$,
is not allowed.
\oRemark

\subhead{Terms}
Raw terms are defined by the grammar
\[
t ::= x \| f \| t\; t' \| \lmd x.t
\]
A term $t$ \W{has a type} \tau if
$t : \tau$
is derived by the following rules:
\[
\infrule{Var}
{x \in V_\tau}{x : \tau}
\qquad
\infrule{Fun}
{f : \tau \in \Sig}
{f : \tau}
\qquad
\infrule{App}
{s : \tau \to \sig\infspc t : \tau}
{s\; t\; : \sig}
\qquad
\infrule{Abs}
{x : \sig \infspc t :\tau}
{\lmd x. t\; : \sig \to \tau}
\]
If a term has a type, then it is \W{well-typed}.
In the sequel, all terms are assumed to be well-typed.

%The notation $\FV(t)$ denotes
%\W{free variables} occurring in a term $t$.

\subhead{\alpha-equivalence}
The notion of \alpha-renaming and \alpha-equivalence between terms are defined as usual, provided
the same variables do not have different types in the terms related by
\alpha-equivalence.

\Remark[rem:alpha]
If one always takes a fresh variable for \alpha-renaming, 
the above side-condition
is satisfied, i.e., there exists a suitable type-indexed family of variables.
\alpha-renaming such as $\lmd y^\fn{int}.\lmd x^\fn{char}.x
=_\alpha \lmd x^\fn{int}.\lmd x^\fn{char}.x$ is possible
only when we use a formulation of type system using
local typing contexts in Church style (i.e. not the present formulation).
\oRemark

\subhead{Substitution}
A substitution $\theta = \set{x_1\mapsto t_1,\ooo, x_n\mapsto t_n}$
is a finite map from variables to terms (not necessary \betaetaNFs), and its application
\[
t\, \theta
\]
is ordinary
capture avoiding substitution that replaces each $x_i$ in a term $t$
with $t_i$.


\subhead{\lmd-calculus}
The simply-typed \lmd-calculus has the \beta-reduction rule 
\[
\arraycolsep = 0mm
\begin{array}[t]{lllllllllll}
&(\beta)\qquad\qquad &
 {(\lmd x.\, {M}) \, {N}} 
\quad &\to  \quad& {M}\{x \mapsto {N}\} \\
\end{array}
\]
The \beta-normal forms are normal forms w.r.t. (\beta).
The simply-typed \lmd-calculus is terminating and confluent.

\subhead{\beta\eta-long normal form}
The notion of \betaetaNF is defined as in 
\cite{Nipkow-TCS}.
Given a term $t$, we denote by $\betanf t$ the \beta-normal form,
$\etanf t$ the \eta-expanded form, and $\betaetanf t$ the \betaetaNF.


%\newpage
\subhead{Higher-order patterns}
A term $t$ is a \W{higher-order pattern}
if $t$ is a \beta-normal form  and
every occurrence of a free variable $F$ in $t$ is of the form
$$F\, s_1\, \ooo\, s_n $$
where $s_1,\ooo,s_n$ are \eta-equivalent to distinct bound variables.



\subhead{Rules}
A \W{rewrite rule} is a pair $l \to r$ such that
\begin{enumerate}
\item $l$ and $r$ are \betaetaNFs of the same base type,
\item $l$ is not a variable,
\item $\FV(l) \supseteq \FV(r)$.
\end{enumerate}

\Remark
Since $l,r$ must be of the base type, $l$ nor $r$ is not binder-headed.
Hence, the case that $l$ is \eta-equivalent to 
a free variable as $\lmd \vec x.M\; \vec x$ is not possible.
\oRemark

\subhead{Rewrite systems}
\begin{itemize}
\item A \W{higher-order rewrite system (HRS)} is given by $(\BB,\set{V_\tau}_{\tau\in\TT},\Sig,\RR)$ 
where \BB is a set of base types,
$\set{V_\tau}_{\tau\in\TT}$ is family of variables,
\Sig is a signature, and \RR is a set of rewrite rules.

\item A \W{pattern rewrite system (PRS)} is a HRS
  $(\BB,\set{V_\tau}_{\tau\in\TT},\Sig,\RR)$, where
every left-hand side is a higher-order pattern.
\end{itemize}



%\newpage
\subhead{Rewriting}
Given an HRS or PRS $(\BB,\set{V_\tau}_{\tau\in\TT},\Sig,\RR)$, 
the one-step rewrite relation $\toR$ on terms is defined as follows:
$
s \toR t 
$
if 
\begin{enumerate} %[nosep]
\item $s$ is a \betaetaNF,
\item there exists a rule $l \to r \in \RR$,
\item there exists a position $p$ of $s$,
\item there exists a substitution $\theta = \set{x_1\mapsto t_1,\ooo, x_n\mapsto t_n}$,
\item ${\subterm{s}p} \,=\, \betaetanf{(l\, \theta)}$,
\item $t  = \replace{s\,}{\betaetanf{r \theta}}p$.
\end{enumerate}

\Remark
Since $\replace{s}{\betaetanf{u}}p
= \betaetanf{(\replace{s}{u}p)}$ holds for a \betaetaNF $s$ and a term $t$, the rewrite relation is defined on only
\betaetaNFs.
\oRemark

\Remark
In a substitution $\theta = \set{x_1\mapsto t_1,\ooo, x_n\mapsto t_n}$,
all $t_i$ are well-typed, hence variables in $t_i$ are taken 
from a variable family $\set{V_\tau}_{\tau\in\TT}$.
\oRemark

\section{How to regard CoCo's HRS format as a mathematical definition of a HRS}
Suppose that a CoCo HRS definition \DD is given.
Let \textbf{VAR} be the set of \s{VAR}-declarations, 
\textbf{FUN} the set of \s{FUN}-declarations, and
\textbf{RULES} the set of \s{FUN}-declarations of \DD.
This induces a higher-order rewrite system as follows.


\begin{itemize}
\item For each type $\tau \in \TT$, suppose
a {countably infinite set} $V_\tau$ of typed variables.
\item Let $\textbf{VAR}_\tau = \set{ x \| (x : \tau)\, \in \textbf{VAR}}$.
Then 
$\textbf{VAR}_\tau \subseteq V_\tau$ must hold for each type $\tau \in \TT$.

\item We define $\Sig = \textbf{FUN}$.
\item We define $\RR = \textbf{RULES}$.
\item All the conditions for a HRS must be satisfied.
\end{itemize}

In CoCo' HRS definition, how to understand $V_\tau$ other than
$\textbf{VAR}_\tau$ is as follows.
In any step of confluence checking,
we need to ensure that we \W{can infer
a suitable variable family} $\set{V_\tau}_{\tau\in\TT}$.
For example, consider the following HRS definition.
\begin{hrs}
(VAR
  x : a
  M : a -> b
  N : a)  
(FUN
  lam : (a -> b) -> Arrab
  app : Arrab -> a -> b     )
(RULES
  app(lam(\x.M x),N) -> M N
  lam(\x.app(M,x))   -> M   )
\end{hrs}
Then, a checker may need to generate a variant of a rule as
\begin{hrs}
  app(lam(\x1.M2 x1),N3) -> M2 N3
\end{hrs}
Although \code{x1,M2,N3} are not listed in the \s{VAR}-declaration,
this is correct generation, because we can infer a variable family as
\begin{align}
  V_{\s a} =\set{\s x, \s N, \s{x1}, \s{N3}, \ooo}, \qquad
  V_{\s{a$\to$ b}} =\set{\s M, \s{M2}, \ooo}
\end{align}
But the following variant is invalid in the present setting
\begin{hrs}
  app(lam(\x.N x),M) -> N M
\end{hrs}
because a variable family cannot be inferred. The variables there are incompatible with the ones in 
the \s{VAR}-declaration.
The inference of variable family need not to be actually implemented
in a checker. This is merely a meta-theoretic requirement in
CoCo's confluence checking process.

\bibliographystyle{plain}
\bibliography{bib}

\end{document}

% Local Variables:
% TeX-master: "hrs"
% TeX-command-default: "2eLaTeX"
% End:
